package com.tacitknowledge.pingpong;

import com.bazaarvoice.dropwizard.assets.ConfiguredAssetsBundle;
import com.tacitknowledge.pingpong.auth.ExampleAuthenticator;
import com.tacitknowledge.pingpong.core.Game;
import com.tacitknowledge.pingpong.core.Player;
import com.tacitknowledge.pingpong.db.GameDAO;
import com.tacitknowledge.pingpong.db.PlayerDAO;
import com.tacitknowledge.pingpong.resources.GameResource;
import com.tacitknowledge.pingpong.resources.PlayerResource;
import com.tacitknowledge.pingpong.resources.ProtectedResource;

import io.dropwizard.Application;
import io.dropwizard.auth.basic.BasicAuthProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class PingPongApplication extends Application<PingPongConfiguration> {
    public static void main(String[] args) throws Exception {
        new PingPongApplication().run(args);
    }

    private final HibernateBundle<PingPongConfiguration> hibernateBundle =
            new HibernateBundle<PingPongConfiguration>(Player.class, Game.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(PingPongConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    @Override
    public String getName() {
        return "pingpong";
    }

    @Override
    public void initialize(Bootstrap<PingPongConfiguration> bootstrap) {
//        bootstrap.addBundle(new AssetsBundle("/assets/app/", "/static", "index.html"));
        bootstrap.addBundle(new ConfiguredAssetsBundle("/assets/app", "/static"));

        bootstrap.addBundle(new MigrationsBundle<PingPongConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(PingPongConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.addBundle(hibernateBundle);
    }

    @Override
    public void run(PingPongConfiguration configuration,
                    Environment environment) throws ClassNotFoundException {
        final PlayerDAO dao = new PlayerDAO(hibernateBundle.getSessionFactory());
        final GameDAO gameDAO = new GameDAO(hibernateBundle.getSessionFactory());

        environment.jersey().register(new BasicAuthProvider<>(new ExampleAuthenticator(),
                                                              "SUPER SECRET STUFF"));
        environment.jersey().register(new ProtectedResource());
        environment.jersey().register(new PlayerResource(dao));
        environment.jersey().register(new GameResource(gameDAO));
//        environment.jersey().setUrlPattern("/application/*");
    }
}
