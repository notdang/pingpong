package com.tacitknowledge.pingpong.db;

import java.util.List;

import org.hibernate.SessionFactory;

import com.tacitknowledge.pingpong.core.Player;
import com.google.common.base.Optional;

import io.dropwizard.hibernate.AbstractDAO;

public class PlayerDAO extends AbstractDAO<Player> {
    public PlayerDAO(SessionFactory factory) {
        super(factory);
    }

    public Optional<Player> findById(Long id) {
        return Optional.fromNullable(get(id));
    }


    public Player create(Player player) {
        return persist(player);
    }

    public List<Player> findAll() {
        return list(namedQuery("com.tacitknowledge.pingpong.core.Person.findAll"));
    }
}
