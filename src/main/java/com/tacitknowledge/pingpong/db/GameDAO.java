package com.tacitknowledge.pingpong.db;

import java.util.List;

import org.hibernate.SessionFactory;

import com.google.common.base.Optional;
import com.tacitknowledge.pingpong.core.Game;

import io.dropwizard.hibernate.AbstractDAO;

public class GameDAO extends AbstractDAO<Game> {
    public GameDAO(SessionFactory factory) {
        super(factory);
    }

    public Optional<Game> findById(Long id) {
        return Optional.fromNullable(get(id));
    }
    public List<Game> findByPlayerId(Long playerId) {
        return list(namedQuery("Game.findByPlayerId").setParameter("id", playerId));
    }

    public Game create(Game game) {
        return persist(game);
    }

    public List<Game> findAll() {
        return list(namedQuery("com.tacitknowledge.pingpong.core.Game.findAll"));
    }
}
