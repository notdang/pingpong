package com.tacitknowledge.pingpong.resources;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.common.base.Optional;
import com.sun.jersey.api.NotFoundException;
import com.tacitknowledge.pingpong.core.Game;
import com.tacitknowledge.pingpong.db.GameDAO;

import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;

@Path("/games")
@Produces(MediaType.APPLICATION_JSON)
public class GameResource {

    private final GameDAO gameDAO;

    public GameResource(GameDAO gameDAO1) {
        this.gameDAO = gameDAO1;
    }

    @POST
    @UnitOfWork
    public Game createGame(Game game) {
        return gameDAO.create(game);
    }

    @GET
    @Path("/{gameId}")
    @UnitOfWork
    public Game getGame(@PathParam("gameId") LongParam gameId) {
        Optional<Game> gameOptional = gameDAO.findById(gameId.get());
        if(!gameOptional.isPresent()){
            throw new NotFoundException("Game not found");
        }
        return gameOptional.get();
    }
    @GET
    @Path("/player/{playerId}")
    @UnitOfWork
    public List<Game> getPlayerGames(@PathParam("playerId") LongParam playerId) {
        return gameDAO.findByPlayerId(playerId.get());
    }
    @PUT
    @Path("/{gameId}")
    @UnitOfWork
    public Game updatePlayer(@PathParam("gameId") LongParam playerId,  Game game) {
        return gameDAO.create(game);
    }

    @GET
    @UnitOfWork
    public List<Game> listGames() {
        return gameDAO.findAll();
    }

}
