package com.tacitknowledge.pingpong.resources;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.common.base.Optional;
import com.sun.jersey.api.NotFoundException;
import com.tacitknowledge.pingpong.core.Player;
import com.tacitknowledge.pingpong.db.PlayerDAO;

import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;

@Path("/players")
@Produces(MediaType.APPLICATION_JSON)
public class PlayerResource {

    private final PlayerDAO playerDAO;

    public PlayerResource(PlayerDAO playerDAO) {
        this.playerDAO = playerDAO;
    }

    @POST
    @UnitOfWork
    public Player createPlayer(Player player) {
        return playerDAO.create(player);
    }

    @GET
    @Path("/{playerId}")
    @UnitOfWork
    public Player getPlayer(@PathParam("playerId") LongParam playerId) {
        Optional<Player> playerOptional = playerDAO.findById(playerId.get());
        if(!playerOptional.isPresent()){
            throw new NotFoundException("Player not found");
        }
        return playerOptional.get();
    }

    @PUT
    @Path("/{playerId}")
    @UnitOfWork
    public Player updatePlayer(@PathParam("playerId") LongParam playerId,  Player player) {
//        Optional<Player> playerOptional = playerDAO.findById(playerId.get());
//        if(!playerOptional.isPresent()){
//            throw new NotFoundException("Player not found");
//        }
        return playerDAO.create(player);
    }

    @GET
    @UnitOfWork
    public List<Player> listPlayers() {
        return playerDAO.findAll();
    }

}
