package com.tacitknowledge.pingpong.core;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "games")
@NamedQueries({
        @NamedQuery(
                name = "com.tacitknowledge.pingpong.core.Game.findAll",
                query = "SELECT p FROM Game p"
        ),
        @NamedQuery(
                name = "Game.findById",
                query = "SELECT p FROM Game p WHERE p.id = :id"
        ),
        @NamedQuery(
                name = "Game.findByPlayerId",
                query = "SELECT p FROM Game p WHERE p.away.id = :id or p.home.id= :id"
        )
})
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "homeScore")
    private int homeScore;

    @Column(name = "awayScore")
    private int awayScore;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = Player.class)
    @JoinColumn(name = "AWAY_ID", nullable = false)
    private Player away;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = Player.class)
    @JoinColumn(name = "HOME_ID", nullable = false)
    private Player home;

    public int getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(int homeScore) {
        this.homeScore = homeScore;
    }

    public int getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(int awayScore) {
        this.awayScore = awayScore;
    }

    public Player getAway() {
        return away;
    }

    public void setAway(Player away) {
        this.away = away;
    }

    public Player getHome() {
        return home;
    }

    public void setHome(Player home) {
        this.home = home;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
