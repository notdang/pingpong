'use strict';

angular.module('tkPingPongChampionshipApp')
.factory('Game', ['$resource', function($resource) {
  return $resource('/games/:verb/:id', {id:'@id'},{
        'update': { method:'PUT' },
        'players':{method:'GET', isArray:true, params:{verb:'player'}}
    });
}]);;
