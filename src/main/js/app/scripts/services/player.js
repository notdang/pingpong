'use strict';

angular.module('tkPingPongChampionshipApp').factory('Player', ['$resource', function($resource) {
  return $resource('/players/:playerId', {playerId:'@id'},{
        'update': { method:'PUT' }
    });
}]);;
