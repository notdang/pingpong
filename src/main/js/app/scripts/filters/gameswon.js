'use strict';

angular.module('tkPingPongChampionshipApp').filter('gamesWon', function () {
    return function (el, player) {
      var gamesWon  = 0;
      if(typeof el  != 'undefined' && el instanceof Array){
          el.forEach(function(current){
            if (current.away.id === player.id){
              gamesWon +=  (current.awayScore > current.homeScore)?1:0;
            }
            if (current.home.id === player.id){
              gamesWon +=  (current.homeScore > current.awayScore)?1:0;
            }
          });
      }
      return gamesWon;
    };
  });
