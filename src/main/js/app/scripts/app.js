'use strict';

angular
.module('tkPingPongChampionshipApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
])
.config(function ($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
    })
    .when('/players/new', {
        templateUrl: 'views/newplayer.html',
        controller: 'NewPlayerCtrl'
    })
    .when('/players/edit/:playerId', {
        templateUrl: 'views/editplayer.html',
        controller: 'EditPlayerCtrl'
    })
    .when('/games/new', {
        templateUrl: 'views/newgame.html',
        controller: 'NewGameCtrl'
    })
    .when('/games', {
      templateUrl: 'views/listgames.html',
      controller: 'ListgamesCtrl'
    })
    .otherwise({
        redirectTo: '/'
    });
});
