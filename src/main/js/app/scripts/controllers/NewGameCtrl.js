'use strict';

angular.module('tkPingPongChampionshipApp')
.controller('NewGameCtrl', function ($scope, $location, Game, Player) {
    $scope.game = new Game();
    $scope.allPlayers = Player.query();

    $scope.saveGame = function(){
        $scope.game.$save(function(){
            $location.path("/");
        });
    }

    $scope.cancel = function(){
        $location.path("/");
    }
});
