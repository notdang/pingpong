'use strict';

angular.module('tkPingPongChampionshipApp')
.controller('EditPlayerCtrl', function ($scope, $location, $routeParams, Player) {
    $scope.player = Player.get({playerId:$routeParams.playerId});


    $scope.savePlayer = function(){
        $scope.player.$update(function(){
            $location.path("/");
        });
    }

    $scope.cancel = function(){
        console.log("23")
        $location.path("/");
    }
});
