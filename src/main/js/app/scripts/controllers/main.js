'use strict';

angular.module('tkPingPongChampionshipApp')
.controller('MainCtrl', function ($scope, Player, Game) {
    $scope.allPlayers = Player.query();
    $scope.allGames = Game.query();
    $scope.gamesFilter = function(el){
        return function(item){
           return true; //item.away.fullName == 'Ciupic'
        };
    };
    $scope.playerSelected = function(player){
      $scope.currentPlayer  = player;
      Game.players({id:player.id}, function(games){
        $scope.currentGames = games;
      })
    }
});
