'use strict';

angular.module('tkPingPongChampionshipApp')
  .controller('ListgamesCtrl', function ($scope, Game) {
    $scope.allGames = Game.query();
    $scope.gamesFilter = function(el){
        return function(item){
           return item.home.fullName == 'Ion John';
        };
    };
  });
