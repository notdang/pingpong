'use strict';

angular.module('tkPingPongChampionshipApp').controller('NewPlayerCtrl', function ($scope, $location, Player) {
  $scope.player = new Player();



  $scope.savePlayer = function(){
    $scope.player.$save(function(){
        $location.path("/");
    });
  }

    $scope.cancel = function(){

      $location.path("/");
    }
});
