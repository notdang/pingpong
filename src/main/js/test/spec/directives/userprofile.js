'use strict';

describe('Directive: UserProfile', function () {

  // load the directive's module
  beforeEach(module('tkPingPongChampionshipAppApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<-user-profile></-user-profile>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the UserProfile directive');
  }));
});
