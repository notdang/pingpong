'use strict';

describe('Filter: gamesWon', function () {

  // load the filter's module
  beforeEach(module('tkPingPongChampionshipAppApp'));

  // initialize a new instance of the filter before each test
  var gamesWon;
  beforeEach(inject(function ($filter) {
    gamesWon = $filter('gamesWon');
  }));

  it('should return the input prefixed with "gamesWon filter:"', function () {
    var text = 'angularjs';
    expect(gamesWon(text)).toBe('gamesWon filter: ' + text);
  });

});
