'use strict';

describe('Controller: ListgamesCtrl', function () {

  // load the controller's module
  beforeEach(module('tkPingPongChampionshipAppApp'));

  var ListgamesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ListgamesCtrl = $controller('ListgamesCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
