'use strict';

describe('Controller: NewplayerCtrl', function () {

  // load the controller's module
  beforeEach(module('tkPingPongChampionshipApp'));

  var NewplayerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewplayerCtrl = $controller('NewPlayerCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });

});
