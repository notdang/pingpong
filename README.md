# Introduction

The drop wizard example application was developed to, as its name implies, provide examples of some of the features
present in drop wizard.

# Overview



# Running The Application

To test the example application run the following commands.

* To package the example run.

        mvn clean package

* To setup the h2 database run.

        java -jar target/pingpong-0.7.1.jar db migrate pingpong.yml

* To run the server run.

        java -jar target/pingpong-0.7.1.jar server pingpong.yml

* Application runs here:

	http://localhost:8080/static/index.html



